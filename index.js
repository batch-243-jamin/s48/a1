// console.log("Add Post");

let posts = [];
let count = 1;

// Add post data

	// addEventListener(event, callback function that will be trigger if the event occur or happen)
	document.querySelector("#form-add-post").addEventListener
	("submit", 
		(event) => {	
			// prevent Default function stops the auto reload of the webpage when submitting	
			event.preventDefault();
			// console.log("Hi")
	
		posts.push({
			id: count,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value
		})

		// console.log(posts);

		count++;
		showPosts(posts);
	});

// Show posts
	
	const showPosts = (post) => {
		let postEntries = '';

		posts.forEach((post) => {
			postEntries += ` <div id = "post-${post.id}"> 
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost('${post.id}')" >Edit</button>
			<button onclick = "deletePost('${post.id}')">Delete</button>
			</div>`
		});

		// console.log(postEntries);

		document.querySelector("#div-post-entries").innerHTML = postEntries;
	}

// Edit post

const editPost = (id)=>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#txt-edit-id').value = id;
	// console.log(title);
	// console.log(body);
}

// Delete post

const deletePost = (id) =>{
	posts.splice(id - 1, 1);
	const deleteSome = document.querySelector(`#post-${id}`);
	deleteSome.remove();
};




document.querySelector("#form-edit-post").addEventListener("submit", 
	(event) => {
	event.preventDefault();

	const idToBeEdited = document.querySelector('#txt-edit-id').value;
	console.log(idToBeEdited);

	for( let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === idToBeEdited){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
			showPosts(posts);
			alert('Edit is successful');
			break;
		}
	}

})

